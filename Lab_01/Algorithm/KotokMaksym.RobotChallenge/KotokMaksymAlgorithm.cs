﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace KotokMaksym.RobotChallenge
{
    public class KotokMaksymAlgorithm : IRobotAlgorithm
    {
        public static double CalcDistance(Position a, Position b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }



        public static int CalcEnergy(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }



        public EnergyStation FindClosestStation(Robot.Common.Robot myRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation station = null;
            double minDistance = 1000;
            foreach(var _station in map.Stations)
            {
                if (CalcDistance(_station.Position, myRobot.Position) < minDistance)
                {
                    station = _station;
                    minDistance = CalcDistance(_station.Position, myRobot.Position);
                }
            }
            return station;
        }



        public static Position OneCellCloser(Position myRobotPosition, Position target)
        {
            Position newPosition = myRobotPosition;
            Position tempPosition;
            double minDistance = CalcDistance(myRobotPosition, target);

            tempPosition = myRobotPosition;
            tempPosition.X -= 1;
            if (CalcDistance(tempPosition, target) < minDistance)
                newPosition = tempPosition;
            tempPosition.X += 2;
            if (CalcDistance(tempPosition, target) < minDistance)
                newPosition = tempPosition;
            tempPosition.X -= 1;
            tempPosition.Y -= 1;
            if (CalcDistance(tempPosition, target) < minDistance)
                newPosition = tempPosition;
            tempPosition.Y += 2;
            if (CalcDistance(tempPosition, target) < minDistance)
                newPosition = tempPosition;
            return newPosition;
        }

        public static Position MoveToPosition(Position myRobotPosition, Position friendPosition)
        {
            if (myRobotPosition.X > friendPosition.X)
                myRobotPosition.X += 4;
            if (myRobotPosition.X > friendPosition.X)
                myRobotPosition.X -= 4;
            if (myRobotPosition.Y > friendPosition.Y)
                myRobotPosition.Y += 4;
            if (myRobotPosition.Y > friendPosition.Y)
                myRobotPosition.Y -= 4;
            return myRobotPosition;
        }


        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var myRobot = robots[robotToMoveIndex];

            /*
            Robot.Common.Robot friend = null;
            foreach (var robot in robots)
            {
                if ((CalcDistance(robot.Position, myRobot.Position) < 5 && robot.OwnerName == "Kotok Maksym") 
                    && CalcDistance(robot.Position, myRobot.Position) > 0.001)
                {
                    friend = robot;
                    break;
                }
            }

            if (friend != null)
            {
                Position newPosition = MoveToPosition(myRobot.Position, friend.Position);
                if (myRobot.Energy > CalcEnergy(myRobot.Position, newPosition))
                    return new MoveCommand() { NewPosition = newPosition };
                else
                    return new MoveCommand() { NewPosition = OneCellCloser(myRobot.Position, newPosition) };
            }
            */

            if (myRobot.Energy > 350 && robots.Count < map.Stations.Count)
            {
                return new CreateNewRobotCommand();
            }

            EnergyStation station = FindClosestStation(myRobot, map, robots);

            if (CalcDistance(station.Position, myRobot.Position) < 2)
            {
                return new CollectEnergyCommand();
            }
            else
            {
                Robot.Common.Robot enemy = null;
                foreach (var robot in robots)
                {
                    if (CalcDistance(robot.Position, station.Position) < 2 && robot.OwnerName != "Kotok Maksym")
                    {
                        enemy = robot;
                        break;
                    }
                }

                if (enemy != null)
                {
                    if (myRobot.Energy > CalcEnergy(myRobot.Position, enemy.Position) + 30)
                        return new MoveCommand() { NewPosition = enemy.Position };
                    else
                        return new MoveCommand() { NewPosition = OneCellCloser(myRobot.Position, enemy.Position) };
                }
                else
                {
                    Position optimalCell = station.Position;
                    double distance = CalcDistance(station.Position, myRobot.Position);
                    for (int i = station.Position.X - 1; i < station.Position.X + 2 && i < 100; i += 1)
                        for (int j = station.Position.Y - 1; j < station.Position.Y + 2 && j < 100; j += 1)
                        {
                            Position pos = station.Position;
                            if (i < 0)
                                continue;
                            if (j < 0)
                                continue;
                            pos.X = i;
                            pos.Y = j;
                            double dist = CalcDistance(pos, myRobot.Position);
                            if (dist < distance)
                                optimalCell = pos;
                        }
                    if (myRobot.Energy > CalcEnergy(myRobot.Position, optimalCell))
                        return new MoveCommand() { NewPosition = optimalCell };
                    else
                        return new MoveCommand() { NewPosition = OneCellCloser(myRobot.Position, optimalCell) };
                }
            }
        }



        public string Author
        {
            get { return "Kotok Maksym"; }
        }
    }
}
